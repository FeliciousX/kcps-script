# Automatic Booking Script for Swinburne KCPS

## Usage

1. Run details.py to populate data.
2. Run test.py to test data.
3. Run book.py on 10am on the Saturday morning that KCPS is open.

## Notes

To make it easier to populate data, create a plain text with format

```
Full Name
Student ID
Contact Number
3 for Unshaded and 2 for Multistorey
-
```

So example for 2 people named Max and FeliciousX:-

```
Max
2233445
0122211341
3
-
FeliciousX
5566312
016822412
2
0
```

Always end with a `0` and always seperate details using a `-` !

Save it, e.g to `input.txt`

Run `$ cat input.txt | details.py `

Viola! Now you can run test.py to test the data.

Output the test data to a html `test.py > something.html` and view it using a browser to check input data.

Finally, schedule when to run `book.py` in order to make the booking.

## TODO

- [ ] Make book.py independant of booking.txt
