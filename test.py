#!/usr/bin/env python
from urllib.request import urlopen
from urllib.parse import urlencode
import json

"""Your user attributes should be changed in data:

fullName should contain your Full Name
studentID should contain your Student ID
contacts should contain your Phone Number
seasonType indicates what kind of parking:
    1 for Motorcycle,
    2 for Multi-story car park,
    3 for Unshaded car park

Note that this program relies on the application website remaining the same.
Even minor changes will break this script."""

def booking():
    print("""\
            <!DOCTYPE html>
            <html lang="en">
            <head>
            <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
            <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">
            <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
            <title>Details</title>
            </head>
            <body>
            <div class="container-fluid">
            <div class="row">
            <div class="col-xs-12 col-md-12 col-sm-12">
            <table class="table">
            <tr>
            <th>No.</th><th>Full Name</th><th>Student ID</th><th>Contacts</th><th>Season Type</th>
            </tr>
            """)

    with open("/home/feliciousx/Public/booking/booking.txt", 'r') as details:
        x = json.load(details)
    
        for i in range(0, len(x)):
            print ('<tr>')
            print('<td>{1}</td><td>{0[fullName]}</td><td>{0[studentID]}</td><td>{0[contacts]}</td>'
                    .format(x[i], i + 1))
            if x[i]['seasonType'] == '3':
                print('<td>Unshaded</td>')
            else:
                print('<td>Multistorey</td>')

            print ('</tr>')

        print("</table></div></div></div></body></html>")

    details.closed
            
if __name__ == "__main__":
    booking()
