#!/usr/bin/env python
import json

def getdetails(details):
    fullName = input("Full Name: ")
    studentID = input("Student ID: ")
    contacts = input("Contact Number: ")
    seasonType = input("Enter choice: \n 2 : Multistorey, 3 : Unshaded \n")

    d = {"fullName" : fullName, "studentID" : studentID, "contacts" : contacts, "seasonType" : seasonType}

    details.append(d)

if __name__ == "__main__":
    details = []

    while True:
        getdetails(details)

        n = input("Enter 0 to exit or any other key to continue: ")

        if "0" in n:
            break

    print (details)

    with open("booking.txt", 'w') as data:
        json.dump(details, data)
    data.closed

